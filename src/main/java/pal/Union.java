package pal;

import java.util.stream.IntStream;

public class Union {

    private int[] parents;
    private int[] ranks;

    private long cost = 0;
    private int addedEdges = 0;

    private Union(int vertexes) {
//        parents = IntStream.rangeClosed(1, vertexes).toArray();
        parents = IntStream.rangeClosed(0, vertexes-1).toArray();
        ranks = IntStream.generate(() -> 0).limit(vertexes).toArray();
    }

    public void addInUnion(Edge edge) {
        addInUnion(edge.getFrom(), edge.getTo(), edge.getLength());
    }

    private void addInUnion(int v1, int v2, long cost) {
        int parent1 = getParent(v1);
        int parent2 = getParent(v2);
        int parent1Rank = getRank(parent1);
        int parent2Rank = getRank(parent2);

        int newParent;
        if (parent2Rank > parent1Rank) {
            newParent = parent2;
            setNewParent(parent1, newParent);
            setNewParent(v1, newParent);
        } else {
            newParent = parent1;
            setNewParent(parent2, newParent);
            setNewParent(v2, newParent);
        }
        if (parent1Rank == parent2Rank) {
            setNewRank(newParent, ++parent1Rank);
        }
        this.cost += cost;
        this.addedEdges++;
    }

    public boolean hasSameParent(Edge edge) {
        return hasSameParent(edge.getFrom(), edge.getTo());
    }

    private boolean hasSameParent(int v1, int v2) {
        return getParent(v1) == getParent(v2);
    }

    private int getParent(int vertex) {
//        int parent = parents[vertex - 1];
        int parent = parents[vertex];
        if (parent == vertex) {
            return parent;
        } else {
            parent = getParent(parent);
//            parents[vertex - 1] = parent;
            parents[vertex] = parent;
            return parent;
        }
    }

    private int getRank(int vertex) {
//        return ranks[vertex - 1];
        return ranks[vertex];
    }

    private void setNewParent(int vertex, int newParent) {
//        parents[vertex - 1] = newParent;
        parents[vertex] = newParent;
    }

    private void setNewRank(int vertex, int newRank) {
//        ranks[vertex - 1] = newRank;
        ranks[vertex] = newRank;
    }

    public static Union ofVertexes(int vertexes) {
        return new Union(vertexes);
    }

    @Override
    public String toString() {
        return "[" + cost + ']';
    }

    public long getCost() {
        return cost;
    }

    public boolean isWorthSearching(long minCost) {
        return cost < minCost;
    }

    public int getAddedEdges() {
        return addedEdges;
    }
}
