package pal;

import java.util.List;

import static pal.Main.vertexesNo;

public class KruskalAlgorithm {

    public static void runKruskalWithUnionFind(Trace trace, Union union) {
        int i = 0;
        Edge[] edges = trace.getVertexArray();
        while (i < edges.length) {
            if (union.getAddedEdges() == vertexesNo - 1) {
                break;
            }

            Edge edge = edges[i++];
            checkAndAddIfDifferentParents(edge, union);
        }
    }

    public static void runKruskalWithUnionFind(List<Edge> edgeList, Union union, long minCost) {
        int i = 0;
        while (i < edgeList.size()) {
            if (union.getAddedEdges() == vertexesNo - 1 || !union.isWorthSearching(minCost)) {
                break;
            }

            Edge edge = edgeList.get(i++);
            checkAndAddIfDifferentParents(edge, union);
        }
    }
//
//    public static void runKruskalWithUnionFind(List<Integer> minLengths, MyHashMultimap<Integer, Edge> multimap, Union union) {
//        int i = 0;
//        while (i < minLengths.size()) {
//            if (union.getAddedEdges() == vertexesNo - 1) {
//                break;
//            }
//            int iteration = minLengths.get(i++);
//            List<Edge> edges = multimap.get(iteration);
//            for (Edge edge : edges) {
//                if (union.getAddedEdges() == vertexesNo - 1) {
//                    break;
//                }
//                checkAndAddIfDifferentParents(edge, union);
//            }
//        }
//    }

    private static void checkAndAddIfDifferentParents(Edge edge, Union union) {
        if (!union.hasSameParent(edge)) {
            union.addInUnion(edge);
        }
    }

}
