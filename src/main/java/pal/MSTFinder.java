package pal;

import java.util.ArrayList;

class MSTFinder {

    private static long minCost = Long.MAX_VALUE;

    static long findMinimalCostMST(ArrayList<Edge> routesArray, ArrayList<Trace> shortestRoutes) {
        for (Trace trace : shortestRoutes) {
            Union union = Union.ofVertexes(Main.vertexesNo);
            KruskalAlgorithm.runKruskalWithUnionFind(trace, union);
            KruskalAlgorithm.runKruskalWithUnionFind(routesArray, union, minCost);
            minCost = (minCost > union.getCost()) ? minCost = union.getCost() : minCost;
        }
        return minCost;
    }
}
