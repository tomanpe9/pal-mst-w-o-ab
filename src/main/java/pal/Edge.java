package pal;

public class Edge implements Comparable<Edge> {

    private final int from;
    private final int to;
    private final long length;

    public Edge(int from, int to, long length) {
        this.from = from;
        this.to = to;
        this.length = length;
    }


    public static Edge fromEdgeArray(String[] edgeArray) {
        return new Edge(Integer.parseInt(edgeArray[0]), Integer.parseInt(edgeArray[1]), Long.parseLong(edgeArray[2]));
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public long getLength() {
        return length;
    }

    public int getOutcommer(int incomer) {
        return incomer == from ? to : from;
    }

    @Override
    public int compareTo(Edge e) {
        return Long.compare(length, e.length);
    }

    @Override
    public String toString() {
        return "[" + from + "," + to + "] = " + length;
    }
}

