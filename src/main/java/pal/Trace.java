package pal;

import java.util.Arrays;

public class Trace implements Comparable<Trace> {

    private Edge[] vertexArray;
    private long price;

    private int nextVertex;

    public Trace() {
    }

    public Trace(Edge[] vertexArray, long price, int nextVertex) {
        this.vertexArray = vertexArray;
        this.price = price;
        this.nextVertex = nextVertex;
    }

    public int getNextVertex() {
        return nextVertex;
    }

    public void setNextVertex(int nextVertex) {
        this.nextVertex = nextVertex;
    }

    public Edge[] getVertexArray() {
        return vertexArray;
    }

    public void fillVertexIndex(Edge e, int index) {
        vertexArray[index] = e;
        price += e.getLength();
    }

    public long getPrice() {
        return price;
    }

    public Trace clone(int nextVertex) {
        return new Trace(this.vertexArray.clone(), this.price, nextVertex);
    }

    @Override
    public String toString() {
        return Arrays.toString(vertexArray) + " = " + price + ", " + nextVertex;
    }

    @Override
    public int compareTo(Trace trace) {
        return Long.compare(price, trace.getPrice()); // Ascending order
    }
}
