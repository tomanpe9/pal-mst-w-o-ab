package pal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import static pal.DistanceFinder.findRoutesAtoB;
import static pal.DistanceFinder.foundDistance;
import static pal.MSTFinder.findMinimalCostMST;

public class Main {

    static int vertexesNo;
    static int edgesNo;
    static int vertexA;
    static int vertexB;

    private static ArrayList<Edge>[] vertexes;
    private static ArrayList<Edge> edges;
//    static MyHashMultimap<Integer, Edge> weightMap;

    /**
     * Running algorithm inside.
     * <p>
     * Reading from System.in and returning solution in System.out
     *
     * @throws IOException on fail to read
     */
    public static void main(String[] args) throws IOException {
        String solution = processSolution(new BufferedReader(new InputStreamReader(System.in)));
        System.out.println(solution);
    }

    /**
     * Moved to new method to allow adding different BufferedReader
     *
     * @param bufferedReader with graph data
     * @return best solution
     * @throws IOException on fail to read
     */
    static String processSolution(BufferedReader bufferedReader) throws IOException {
        final ArrayList<Edge>[] data = processBufferedReaderExample(bufferedReader);
        final ArrayList<Trace> routesAtoBArray = findRoutesAtoB(data);
        routesAtoBArray.sort(Trace::compareTo);
        long minimalCost = findMinimalCostMST(edges, routesAtoBArray);
        return foundDistance + " " + minimalCost;
    }

    /**
     * Simple example how to read from buffered reader
     *
     * @param bufferedReader buffered reader
     * @throws IOException on fail with buffered reader
     */
    private static ArrayList<Edge>[] processBufferedReaderExample(BufferedReader bufferedReader) throws IOException {
        String[] summaryStr = bufferedReader.readLine().split(" ");

        vertexesNo = Integer.parseInt(summaryStr[0]);
        edgesNo = Integer.parseInt(summaryStr[1]);
        vertexA = Integer.parseInt(summaryStr[2]) - 1;
        vertexB = Integer.parseInt(summaryStr[3]) - 1;

        vertexes = new ArrayList[vertexesNo];
        for (int i = 0; i < vertexesNo; i++) {
            vertexes[i] = new ArrayList<>();
        }
//        weightMap = new MyHashMultimap<>();
        edges = new ArrayList<>(edgesNo);

        while (bufferedReader.ready()) {
            String line = bufferedReader.readLine();
            if (line == null) {
                break;
            }
            String[] split = line.split(" ");

            Integer v1 = Integer.parseInt(split[0]) - 1;
            Integer v2 = Integer.parseInt(split[1]) - 1;
            Integer w = Integer.parseInt(split[2]);

            Edge edge = new Edge(v1, v2, w);//Edge.fromEdgeArray(split);
            vertexes[v1].add(edge);
            vertexes[v2].add(edge);
            edges.add(edge);
//            weightMap.putValue(w, edge);
        }
        edges.sort(Edge::compareTo);
        return vertexes;
    }
}

