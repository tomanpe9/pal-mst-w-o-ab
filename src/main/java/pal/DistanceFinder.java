package pal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static pal.Main.*;

class DistanceFinder {

    static int foundDistance = 0;

    private static int startVertex = vertexB;
    private static int finalVertex = vertexA;

    static ArrayList<Trace> findRoutesAtoB(ArrayList<Edge>[] data) {
        // region init data
        int[] vertexSeen = new int[vertexesNo];
        HashMap<Integer, Edge>[] vertexPredecessors = new HashMap[vertexesNo];
        ArrayList<Integer> thisIteration = new ArrayList<>();
        thisIteration.add(startVertex);
        vertexSeen[startVertex] = -1;
        int iteration = 1;
        // endregion
        while (hasNotFoundSolutionYet()) { // B to A run
            ArrayList<Integer> nextIteration = new ArrayList<>();
            for (int vertex : thisIteration) {
                for (Edge nextVertexEdge : data[vertex]) {
                    int nextVertex = nextVertexEdge.getOutcommer(vertex);

                    if (isFinalVertex(nextVertex)) {
                        if (wasNotSeenBefore(vertexSeen, nextVertex)) {
                            vertexSeen[nextVertex] = iteration;
                            vertexPredecessors[nextVertex] = new HashMap<>();
                            nextIteration.add(nextVertex);
                            foundDistance = iteration;
                        }
                        vertexPredecessors[finalVertex].put(vertex, nextVertexEdge);
                    } else if (hasNotFoundSolutionYet()
                            && wasNotSeenUntilThisIteration(vertexSeen, iteration, nextVertex)) {
                        if (wasNotSeenBefore(vertexSeen, nextVertex)) {
                            vertexSeen[nextVertex] = iteration;
                            vertexPredecessors[nextVertex] = new HashMap<>();
                            nextIteration.add(nextVertex);
                        }
                        vertexPredecessors[nextVertex].put(vertex, nextVertexEdge);
                    }

                }
            }
            if (hasNotFoundSolutionYet()) {
                thisIteration = nextIteration;
                ++iteration;
            }
        }
        return decomposeTrace(vertexPredecessors);
    }

    private static ArrayList<Trace> decomposeTrace(HashMap<Integer, Edge>[] vertexPredecessors) {
        ArrayList<Trace> routesAtoB = new ArrayList<>();
        routesAtoB.add(new Trace(new Edge[foundDistance], 0, finalVertex));
        for (int i = 0; i < foundDistance; i++) { // A to B run
            ArrayList<Trace> nextIteration = new ArrayList<>();
            for (Trace trace : routesAtoB) {
                for (Map.Entry<Integer, Edge> entry : vertexPredecessors[trace.getNextVertex()].entrySet()) {
                    Trace nextTrace = trace.clone(entry.getKey());
                    nextTrace.fillVertexIndex(entry.getValue(), i);
                    nextIteration.add(nextTrace);
                }
//                System.out.println(trace);
            }
            routesAtoB = nextIteration;
        }
        return routesAtoB;
    }

    private static boolean isFinalVertex(int nextVertex) {
        return nextVertex == finalVertex;
    }

    private static boolean hasNotFoundSolutionYet() {
        return foundDistance == 0;
    }

    private static boolean wasNotSeenUntilThisIteration(int[] vertexSeen, int iteration, int vertex) {
        return wasNotSeenBefore(vertexSeen, vertex) || wasSeenThisIteration(vertexSeen, iteration, vertex);
    }

    private static boolean wasNotSeenBefore(int[] vertexSeen, int vertex) {
        return vertexSeen[vertex] == 0;
    }

    private static boolean wasSeenThisIteration(int[] vertexSeen, int iteration, int vertex) {
        return vertexSeen[vertex] == iteration;
    }
}
