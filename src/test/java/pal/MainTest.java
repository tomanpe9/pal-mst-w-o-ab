package pal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Test;

/**
 * Class for tests of Main class.
 * <p>
 * To have correct public data sets just insert them into <i>test/resources/</i> package.
 * As an example there is first file of public data set from assignment 2.
 */
public class MainTest {

	@Test
	public void public1() throws IOException, URISyntaxException {
		String index = "01";
		testImplementationWithDataSetId(index);
	}

	@Test
	public void public2() throws IOException, URISyntaxException {
		String index = "02";
		testImplementationWithDataSetId(index);
	}

	@Test
	public void public3() throws IOException, URISyntaxException {
		String index = "03";
		testImplementationWithDataSetId(index);
	}

	@Test
	public void public4() throws IOException, URISyntaxException {
		String index = "04";
		testImplementationWithDataSetId(index);
	}

	@Test
	public void public5() throws IOException, URISyntaxException {
		String index = "05";
		testImplementationWithDataSetId(index);
	}

	@Test
	public void public6() throws IOException, URISyntaxException {
		String index = "06";
		testImplementationWithDataSetId(index);
	}

	@Test
	public void public7() throws IOException, URISyntaxException {
		String index = "07";
		testImplementationWithDataSetId(index);
	}

	@Test
	public void public8() throws IOException, URISyntaxException {
		String index = "08";
		testImplementationWithDataSetId(index);
	}

	@Test
	public void public9() throws IOException, URISyntaxException {
		String index = "09";
		testImplementationWithDataSetId(index);
	}

	@Test
	public void public10() throws IOException, URISyntaxException {
		String index = "10";
		testImplementationWithDataSetId(index);
	}

	private void testImplementationWithDataSetId(String index) throws IOException {
		System.out.println("\nReading file #" + index);

		BufferedReader fileIn = new BufferedReader(getResourceFileInStreamReader(index, true));
		BufferedReader fileOut = new BufferedReader(getResourceFileInStreamReader(index, false));

		String solution = Main.processSolution(fileIn);
		System.out.print("Result " + solution);
		Assert.assertEquals(fileOut.readLine(), solution);
		System.out.println(" was correct.");
	}

	private Reader getResourceFileInStreamReader(String index, boolean isIn) {
		String fileName = "/pub" + index + (isIn ? ".in" : ".out");
		InputStream stream = getClass().getResourceAsStream(fileName);
		return new InputStreamReader(stream);
	}
}
